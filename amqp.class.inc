<?php
/**
 * AMQPInstance Class
 *
 * This is simply a wrapper around a connection, channel and exchange
 * to make interacting with the AMQP server easier.
 */

class AMQPInstance {

  private $drupal;

  function __construct($drupal) {
    $this->drupal = $drupal;
  }

  function publish($msg) {
    $vhost = urlencode($this->get_exchanges($msg->exchange)->vhost);
    $path = $this->drupal->variable_get('amqp_http_api', '') . "/exchanges/{$vhost}/{$msg->exchange}/publish";
    $data = array(
      'properties' => array(),
      'routing_key' => $msg->routing_key,
      'payload' => $msg->body,
      'payload_encoding' => 'string',
    );
    return $this->drupal->drupal_http_request($path, array(), 'POST', json_encode($data));
  }

  function get_exchanges($name = NULL) {
    $path = $this->drupal->variable_get('amqp_http_api', '') . "/exchanges";
    $exchanges = json_decode($this->drupal->drupal_http_request($path, array(), "GET")->data);
    if (isset($name)) {
      foreach ($exchanges as $exchange) {
        if ($exchange->name == $name) {
          return $exchange;
        }
      }
    }
    return $exchanges;
  }



}


class AMQPClassInstance {

  protected static $connection = NULL;
  protected static $channel = NULL;
  protected static $exchanges = array();
  protected static $queues = array();

  public static function get_connection() {
    if (!isset(self::$connection)) {
      self::$connection = new AMQPConnection(amqp_connection_args());
      self::$connection->connect();
      self::$channel = new AMQPChannel(self::$connection);
    }

    return self::$connection;
  }

  public static function get_channel() {
    self::get_connection();
    return self::$channel;
  }

  public static function get_exchange($name) {
    if (!array_key_exists($name, self::$exchanges)) {
      $exchange = new AMQPExchange(self::get_channel());

      // If the exchange name is '', then we want the default exchange,
      // which doesn't need to be initialized at all.
      if ($name != '') {
        $info = amqp_get_exchange_info($name);
        try {
          $exchange->setName($name);
          $exchange->setArguments($info['arguments']);
          $exchange->setFlags($info['flags']);
          $exchange->setType($info['type']);
          $exchange->declare();
        }
        catch (Exception $e) {
          watchdog('amqp', 'Unable to declare exchange: %error', array('%error' => $e->getMessage()));
        }
      }

      self::$exchanges[$name] = $exchange;
    }

    return self::$exchanges[$name];
  }

  public static function get_queue($name) {
    if (!array_key_exists($name, self::$queues)) {
      $info = amqp_get_queue_info($name);

      // initialize the exchange
      self::get_exchange($info['exchange']);

      // declare the queue
      $queue = new AMQPQueue(self::get_channel());
      $queue->setName($name);
      $queue->setArguments($info['arguments']);
      $queue->setFlags($info['flags']);
      $queue->declare();

      // bind it to it's exchange
      // TODO: we should probably allow multiple exchanges!
      if ($info['exchange'] != '') {
        $queue->bind($info['exchange'], $info['routing_key']);
      }

      self::$queues[$name] = $queue;
    }

    return self::$queues[$name];
  }
}
