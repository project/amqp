<?php

drupal_queue_include();

class AMQDrupalQueue implements DrupalQueueInterface {
  /**
   * The name of the queue this instance is working with.
   *
   * @var string
   */
  protected $name;

  public function __construct($name) {
    $this->name = $name;
  }

  public function createItem($data) {
    // force the creation of the queue
    return amqp_publish('', $this->name, serialize($data));
  }

  public function numberOfItems() {
    // TODO: is there a way to get this?
    return 1;
  }

  public function claimItem($lease_time = 3600) {
    // TODO: we need a clever way to implement the lease time... We might
    // be able to do something with register_shutdown_function() to simply
    // nack() all unacked messages?

    $queue = AMQPClassInstance::get_queue($this->name);
    if ($message = $queue->get(AMQP_NOPARAM)) {
      $item = array(
        'item_id' => $message->getDeliveryTag(),
        'data' => $message->getBody(),
      );
      return $item;
    }
    return FALSE;
  }

  public function deleteItem($item) {
    // TODO: handle exceptions
    $queue = AMQPClassInstance::get_queue($this->name);
    // Maybe try pulling it as object and/or as array?
    $queue->ack($item['item_id']);
  }

  public function releaseItem($item) {
    // TODO: handle exceptions
    $queue = AMQPClassInstance::get_queue($this->name);
    $queue->nack($item['item_id']);
  }

  public function createQueue() {
    // created on demand
    AMQPClassInstance::get_queue($this->name);
  }

  public function deleteQueue() {
    // TODO: handle exceptions
    AMQPClassInstance::get_queue($this->name)->delete();
  }
}

