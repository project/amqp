<?php

/**
 * Admin settings form for AMQP module
 */
function amqp_admin_form() {
  $installed = amqp_installed(TRUE);
  $form = array();

  $form['amqp_host'] = array(
    '#type' => 'textfield',
    '#title' => t('AMQP host'),
    '#description' => t('Hostname (URI) of AMQP server.'),
    '#default_value' => amqp_variable_host(),
  );
  $form['amqp_port'] = array(
    '#type' => 'textfield',
    '#title' => t('AMQP port'),
    '#description' => t('Port of AMQP server.'),
    '#default_value' => amqp_variable_port(),
  );
  $form['amqp_http_api'] = array(
    '#type' => 'textfield',
    '#title' => t('AMQP HTTP API endpoint'),
    '#description' => t('HTTP API endpoint of AMQP server. If you provide this, the AMQP module can acquire other information about the AMQP server.'),
    '#default_value' => amqp_variable_http_api(),
  );
  $form['amqp_login'] = array(
    '#type' => 'textfield',
    '#title' => t('AMQP login'),
    '#description' => t('Username / Login for AMQP server.'),
    '#default_value' => amqp_variable_login(),
  );
  $form['amqp_password'] = array(
    '#type' => 'textfield',
    '#title' => t('AMQP password'),
    '#description' => t('Password for above user.'),
    '#default_value' => amqp_variable_password(),
  );
  $form['amqp_test_connection'] = array(
    '#type' => 'button',
    '#value' => t('Test Connection with AMQP server'),
    '#executes_submit_callback' => TRUE,
    '#disabled' => !$installed,
    '#submit' => array('amqp_admin_form_submit'),
    '#description' => t('After saving above settings, click this button to test the AMQP server connection.')
  );
  $queue_class = variable_get('queue_default_class', 'SystemQueue');
  $queue_class_options = array(
    'SystemQueue' => t('Drupal system queue'),
    'AMQDrupalQueue' => t('AMQ'),
  );
  if (!in_array($queue_class, array_keys($queue_class_options))) {
    $queue_class_options[$queue_class] = t('Other (%name)', array('%name' => $queue_class));
  }
  $form['queue_default_class'] = array(
    '#type' => 'radios',
    '#title' => t('Drupal queue'),
    '#description' => t('Allows you to make AMQ the default handler for the internal Drupal Queue.'),
    '#options' => $queue_class_options,
    '#default_value' => $queue_class,
  );

  return system_settings_form($form);
}

function amqp_admin_form_submit($form, &$form_state) {
  // amqp_update_exchanges();

  $msg = t('This is a test message from the AMQP Drupal module. If you are reading this, it means the module has been properly configured.');

  amqp_publish('amqp.test', 'amqp.test.routing.key', $msg);

  $queue = drupal_queue_get('amqp_test_queue');
  $message = $queue->claimItem();
  $queue->deleteItem($message);
  drupal_set_message(t("Received message back via MQ: @received", array('@received' => print_r($message, 1))));
}

/**
 * Test message consumer callback
 */
function amqp_test_message_consumer($message) {
  // This would be used to process the message. If you need to provide user-level messages,
  // you should enable (and configure) the amqp_nodejs module.
  error_log('AMQP Message received: ' . $message);
  return "AMQP Test Message consumed.";
}

function amqp_connection_args() {
  return array(
    'host' => amqp_variable_host(),
    'port' => amqp_variable_port(),
    'login' => amqp_variable_login(),
    'password' => amqp_variable_password(),
  );
}

function amqp_variable_host() {
  return variable_get('amqp_host', 'localhost');
}

function amqp_variable_port() {
  return variable_get('amqp_port', 5672);
}

function amqp_variable_http_api() {
  return variable_get('amqp_http_api', 'http://localhost:55672/api');
}

function amqp_variable_login() {
  return variable_get('amqp_login', 'guest');
}

function amqp_variable_password() {
  return variable_get('amqp_password', 'guest');
}
